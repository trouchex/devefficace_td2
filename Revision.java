import java.util.Arrays;

public class Revision {


    public static double puissance(double x, int n) {
        if (n==1) {
            return x;
        }
        return x * puissance(x, n - 1);
    }


    public static void resoudreAux(int n, int depart, int millieu, int arrive) {
        if (n==1) {
            System.out.println(depart + "->" +arrive);
        }
        else {
            resoudreAux(n - 1, depart, arrive, millieu);
            System.out.println(depart+"->"+arrive);
            resoudreAux(n-1, millieu, depart, arrive);
        }
    }

    public static void resoudre(int n) {
        if (n==0) {
            System.out.println("pas de disques");
        }
        resoudreAux(n, 1,2,3);
    }

    // maintenenat le tric sérieux l'histoire du tri avec les danse

    public static void echange(int []t, int i, int j) {
        assert i != j;
        assert j > i;

        int valeurI = t[i];
        int valeurJ = t[j];

        t[i] = valeurJ;
        t[j] = valeurI;
    }


    public static int pivot(int[] t, int i, int j) {
        int pivot = t[j];

        for (int k = i; k < t.length; k++) {
            if (t[k] < pivot) {
                echange(t, i, k);
                i++;
            }
        }
        echange(t, i, j);
        return i;
    }

    public static void quisortAux(int []t, int i, int j) {
        if (i < j) {
            int pivot = pivot(t, i, j);
            quisortAux(t, i, pivot - 1);
            quisortAux(t , pivot + 1, j);
        }
    }

    public static void quickSort(int []t) {
        if (t.length !=0) {
            quisortAux(t, 0 , t.length - 1);
        }
    }

    public static void main(String[] args) {
        /*resoudreAux(2,2,3,1);
        resoudre(5);*/

        int [] t= {
                1,2,5,4,8,45,87,45,6,86,52,8546,23,45,1
        };
        quickSort(t);
        System.out.println(Arrays.toString(t));
    }
}
