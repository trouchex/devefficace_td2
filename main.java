import java.util.Arrays;

public class main {

    public static double puissance(double x , int n) {
        assert  ( n >= 0);
        if (n == 0) {
            return 1;
        }
        else {
            if (n%2 == 0) {
                double r = puissanceRapide(x, n / 2);
                return r * r;
            }
            else {
                double r = puissanceRapide(x, n / 2);
                return r * r * x;
            }
        }
    }

    public static double puissanceRapide(double x , int n) {
        if (n == 1) {
            return x;
        }
        return x * puissanceRapide(x, n - 1);
    }

    public static void echange(int i, int j, int []t) {
        if (t.length != 0) {
            int valeurI = t[i];
            int valeurJ = t[j];
            t[i] = valeurJ;
            t[j] = valeurI;
        }
    }
    public static int pivot(int []t, int i, int j) {
        int pivot = t[j];

        assert (t.length > 0);
        assert (i >= 0 && j < t.length);
        assert (j >= i && j < t.length);

        for (int k = i; k < j; k++) {
            if (t[k] < pivot) {
                echange(i, k, t);
                i++;
            }
        }
        echange(i, j, t);
        return i;
    }

    public static void quickSortAux(int []t, int i, int j) {
        if (i < j) {
            int p = pivot(t, i, j);
            quickSortAux(t, i, p - 1);
            quickSortAux(t, p + 1, j);
        }
    }

    public static void quickSort(int []t) {
        quickSortAux(t, 0, t.length - 1);
    }


    public static void main(String[] args){
        double x = 1.0000001;
        int n = 5000;
        System.out.println("n="+n);
        long t1 = System.currentTimeMillis();
        puissanceRapide(x,n);
        long t2 = System.currentTimeMillis();
        System.out.println("Rapide: "+ (t2-t1) + "ms");
        puissance(x,n);
        System.out.println("Normal: "+ (System.currentTimeMillis()-t2) + "ms");
        System.out.println(puissanceRapide(x , n));

        //System.out.println(pivot(new int[]{0,3,2,1,7,8,9,4}, 0, 7));
        /*int []t = new int[]{0,3,2,1,7,8,9,4};
        int []bigTabPasTrie = new int[]{
                157,  12,  44,  58,  96,  55,  30,  29,  53,  92,  78,  83,  14,  24,  72,  39,  47,  98,  74,  99,
                12,54,87,87,56,897,4,87,894,5,7,5,897
        };
        //quickSort(t);
        quickSort(bigTabPasTrie);
        System.out.println(Arrays.toString(bigTabPasTrie));*/
    }
}
